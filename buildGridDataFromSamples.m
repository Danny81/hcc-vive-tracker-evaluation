samples = 135;
totalDegreesOdFreedom = 6;
numberOfSampleFiles = 32;
mode = 'tabletBack'; %trackerOnly tabletTop tabletBack

varNames = {'x','y', 'z','pitch','yaw','roll'} ;
varTypes = {'double','double','double','double','double','double'} ;
lines = [1 samples];
importOpts = delimitedTextImportOptions('VariableNames', varNames, 'VariableTypes', varTypes, 'Delimiter',';', 'DataLines', lines);
data = zeros(numberOfSampleFiles,samples,totalDegreesOdFreedom);

for file = 1:numberOfSampleFiles
    currFile = readmatrix(strcat('data/positional_accuracy_and_jitter/',mode,'/4matlab/', num2str(file), '.txt'), importOpts);
    aroundZero = zeros(6);
    for iSample=1:samples
        for degreeOfFreedom=1:totalDegreesOdFreedom
            currentValue = currFile(iSample, degreeOfFreedom);
            if abs(currentValue) >=0 &&  abs(currentValue) <=20
                aroundZero(degreeOfFreedom)=1;
            end
            if aroundZero(degreeOfFreedom) == 1 && currentValue >= 350
               currentValue = 360 - currentValue; 
            end
           %data(file, iSample , degreeOfFreedom) = currentValue;
           %remap pitch yaw roll order to niehorster's matlab script
           realDegree = degreeOfFreedom;
           if degreeOfFreedom == 4 %Unity X angle ===> pitch
                realDegree = 5;
            elseif degreeOfFreedom == 5 %Unity Y angle ===> roll
                realDegree = 6;
           elseif realDegree == 6 %Unity z angle ===> yaw
               realDegree = 4;
           else
               realDegree = degreeOfFreedom;
            end
           data(file, iSample , realDegree) = currentValue;
           
        end
    end
end