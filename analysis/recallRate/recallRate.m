close all
clc;

load(strcat('../../data/recallRate/data.mat'));
resultDir    = strcat('../../results/recallRate/');

attempts = 40;

totalPosDetailsOnDemandTop = sum (detailsOnDemandDetectedTop);
totalPosDetailsOnDemandBack = sum (detailsOnDemandDetectedBack);
totalPosTabletToWallScreenTop = sum (tabletToWallScreenDetectedTop, 'all');
totalPosTabletToWallScreenBack = sum (tabletToWallScreenDetectedBack, 'all');

totalRecallDetailsOnDemandTop = totalPosDetailsOnDemandTop / (totalPosDetailsOnDemandTop + attempts*numel(detailsOnDemandDetectedTop) - totalPosDetailsOnDemandTop);
totalRecallDetailsOnDemandBack = totalPosDetailsOnDemandBack / (totalPosDetailsOnDemandBack + attempts*numel(detailsOnDemandDetectedBack) - totalPosDetailsOnDemandBack);

totalRecallTabletToWallScreenTop =  totalPosTabletToWallScreenTop / (  totalPosTabletToWallScreenTop + attempts*numel(tabletToWallScreenDetectedTop) - totalPosTabletToWallScreenTop);
totalRecallTabletToWallScreenBack = totalPosTabletToWallScreenBack / ( totalPosTabletToWallScreenBack + attempts*numel(tabletToWallScreenDetectedBack) - totalPosTabletToWallScreenBack);

recallTabletToWallScreenTopPerCoord = zeros (4,5);
recallTabletToWallScreenBackPerCoord = zeros (4,5);

recallDetailsOnDemandTopPerCoord = zeros (1,3);
recallDetailsOnDemandBackPerCoord = zeros (1,3);

for z = 1:4
    for x = 1:5
        recallTabletToWallScreenTopPerCoord(z,x) =  tabletToWallScreenDetectedTop(z,x) / (tabletToWallScreenDetectedTop(z,x) + attempts - tabletToWallScreenDetectedTop(z,x));
        recallTabletToWallScreenBackPerCoord(z,x) =  tabletToWallScreenDetectedBack(z,x) / (tabletToWallScreenDetectedBack(z,x) + attempts - tabletToWallScreenDetectedBack(z,x));
    end
end

for z = 1:3
    recallDetailsOnDemandTopPerCoord(1,z) = detailsOnDemandDetectedTop(1, z) / (detailsOnDemandDetectedTop(1,z) + attempts - detailsOnDemandDetectedTop(1,z));
    recallDetailsOnDemandBackPerCoord(1,z) = detailsOnDemandDetectedBack(1, z) / (detailsOnDemandDetectedBack(1,z) + attempts - detailsOnDemandDetectedBack(1,z));
end

fprintf('Recall Rate Details-On-Demand Tracker Top = %f m\n', totalRecallDetailsOnDemandTop);
fprintf('Recall Rate Details-On-Demand Tracker Back = %f m\n', totalRecallDetailsOnDemandBack);
fprintf('Recall Rate Tablet-to-Wall-Screen Tracker Top = %f m\n', totalRecallTabletToWallScreenTop);
fprintf('Recall Rate Tablet-to-Wall-Screen Tracker Back = %f m\n', totalRecallTabletToWallScreenBack);

xlabels = {-2, -1, 0, 1, 2};
ylabelsTabletToWallDispay = {1.5, 1, 0.5, 0};
xlabelsDetailsOnDemand = {-1, 0, 1};
ylabelsDetailsOnDemand = {1.5};


figure
h = heatmap(xlabels, ylabelsTabletToWallDispay, recallTabletToWallScreenTopPerCoord);
caxis([0.5 1]);
h.Title = 'Recall Rate Tablet-To-Wall-Screen Tracker Top';
h.XLabel = 'X-Coordinate';
h.YLabel = 'Z-Coordinate';
print([resultDir '\recallTabletToWallScreenTopPerCoord.png'],'-dpng','-r300')

figure
h = heatmap(xlabels, ylabelsTabletToWallDispay, recallTabletToWallScreenBackPerCoord);
caxis([0.5 1]);
h.Title = 'Recall Rate Tablet-To-Wall-Screen Tracker Backside';
h.XLabel = 'X-Coordinate';
h.YLabel = 'Z-Coordinate';
print([resultDir '\recallTabletToWallScreenBackPerCoord.png'],'-dpng','-r300')

figure
h = heatmap(xlabelsDetailsOnDemand, ylabelsDetailsOnDemand, recallDetailsOnDemandTopPerCoord);
caxis([0.8 1]);
h.Title = 'Recall Rate Details-on-Demand Tracker Top';
h.XLabel = 'X-Coordinate';
h.YLabel = 'Z-Coordinate';
print([resultDir '\recallDetailsOnDemandTopPerCoord.png'],'-dpng','-r300')

figure
h = heatmap(xlabelsDetailsOnDemand, ylabelsDetailsOnDemand, recallDetailsOnDemandBackPerCoord);
caxis([0.8 1]);
h.Title = 'Recall Rate Details-on-Demand Tracker Backside';
h.XLabel = 'X-Coordinate';
h.YLabel = 'Z-Coordinate';
print([resultDir '\recallDetailsOnDemandBackPerCoord.png'],'-dpng','-r300')

[p,h] = ranksum(recallDetailsOnDemandTopPerCoord,recallDetailsOnDemandBackPerCoord);

vec1 = recallTabletToWallScreenTopPerCoord(:);
vec2 = recallTabletToWallScreenBackPerCoord(:);
[a, b] = ranksum(vec1,vec2);


