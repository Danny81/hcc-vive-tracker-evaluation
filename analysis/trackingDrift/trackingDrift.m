close all
clc;

load(strcat('../../data/trackingDrift/data.mat'));
resultDir    = strcat('../../results/trackingdrift/');

numSamples = 11916;

totalDriftTrackerOnly = 0;
totalDriftTrackerTop = 0;
totalDriftTrackerBack = 0;

changeXtrackerOnly = zeros(numSamples-1,1);
changeYtrackerOnly = zeros(numSamples-1,1);
changeZtrackerOnly = zeros(numSamples-1,1);

changeXtrackerTop = zeros(numSamples-1,1);
changeYtrackerTop = zeros(numSamples-1,1);
changeZtrackerTop = zeros(numSamples-1,1);

changeXtrackerBack = zeros(numSamples-1,1);
changeYtrackerBack = zeros(numSamples-1,1);
changeZtrackerBack = zeros(numSamples-1,1);

samplesTop = zeros(numSamples,3);
samplesBack = zeros(numSamples,3);
samplesTrackerOnly = zeros(numSamples,3);

for currSample = 1:numSamples
    samplesTop(currSample, 1:3) = dataTrackerTop(currSample, 1:3);
    samplesBack(currSample, 1:3) = dataTrackerBack(currSample, 1:3);
    samplesTrackerOnly(currSample, 1:3) = dataTrackerOnly(currSample, 1:3);
    if currSample > 1      
        totalDriftTrackerOnly = totalDriftTrackerOnly + norm(previousPointTrackerOnly - dataTrackerOnly(currSample,1:3)); 
        totalDriftTrackerTop = totalDriftTrackerTop + norm(previousPointTrackerTop - dataTrackerTop(currSample,1:3));
        totalDriftTrackerBack = totalDriftTrackerBack + norm(previousPointTrackerBack - dataTrackerBack(currSample,1:3)); 
        
        CurrentChangeXtrackerOnly = abs(previousPointTrackerOnly(1,1) - dataTrackerOnly(currSample,1))*1000;
        if dataTrackerOnly(currSample,1) < previousPointTrackerOnly(1,1)
           CurrentChangeXtrackerOnly=-CurrentChangeXtrackerOnly; 
        end
        changeXtrackerOnly(currSample-1)= CurrentChangeXtrackerOnly;       
        CurrentChangeXtrackerTop = abs(previousPointTrackerTop(1,1) - dataTrackerTop(currSample,1))*1000;
        if dataTrackerTop(currSample,1) < previousPointTrackerTop(1,1)
           CurrentChangeXtrackerTop=-CurrentChangeXtrackerTop; 
        end
        changeXtrackerTop(currSample-1)= CurrentChangeXtrackerTop;
        %
        CurrentChangeXtrackerBack = abs(previousPointTrackerBack(1,1) - dataTrackerBack(currSample,1))*1000;
        if dataTrackerBack(currSample,1) < previousPointTrackerBack(1,1)
           CurrentChangeXtrackerBack=-CurrentChangeXtrackerBack; 
        end
        changeXtrackerBack(currSample-1)= CurrentChangeXtrackerBack;
        
        CurrentChangeYtrackerOnly = abs(previousPointTrackerOnly(1,2) - dataTrackerOnly(currSample,2))*1000;
        if dataTrackerOnly(currSample,2) < previousPointTrackerOnly(1,2)
           CurrentChangeYtrackerOnly=-CurrentChangeYtrackerOnly; 
        end
        changeYtrackerOnly(currSample-1)= CurrentChangeYtrackerOnly;
        CurrentChangeYtrackerTop = abs(previousPointTrackerTop(1,2) - dataTrackerTop(currSample,2))*1000;
        if dataTrackerTop(currSample,2) < previousPointTrackerTop(1,2)
           CurrentChangeYtrackerTop=-CurrentChangeYtrackerTop; 
        end
        changeYtrackerTop(currSample-1)= CurrentChangeYtrackerTop;
        %
        CurrentChangeYtrackerBack = abs(previousPointTrackerBack(1,2) - dataTrackerBack(currSample,2))*1000;
        if dataTrackerBack(currSample,2) < previousPointTrackerBack(1,2)
           CurrentChangeYtrackerBack=-CurrentChangeYtrackerBack; 
        end
        changeYtrackerBack(currSample-1)= CurrentChangeYtrackerBack;
        
        CurrentChangeZtrackerOnly = abs(previousPointTrackerOnly(1,3) - dataTrackerOnly(currSample,3))*1000;
        if dataTrackerOnly(currSample,2) < previousPointTrackerOnly(1,2)
           CurrentChangeZtrackerOnly=-CurrentChangeZtrackerOnly; 
        end
        changeZtrackerOnly(currSample-1)= CurrentChangeZtrackerOnly;
        CurrentChangeZtrackerTop = abs(previousPointTrackerTop(1,3) - dataTrackerTop(currSample,3))*1000;
        if dataTrackerTop(currSample,2) < previousPointTrackerTop(1,2)
           CurrentChangeZtrackerTop=-CurrentChangeZtrackerTop; 
        end
        changeZtrackerTop(currSample-1)= CurrentChangeZtrackerTop;
        %
        CurrentChangeZtrackerBack = abs(previousPointTrackerBack(1,3) - dataTrackerBack(currSample,3))*1000;
        if dataTrackerBack(currSample,2) < previousPointTrackerBack(1,2)
           CurrentChangeZtrackerBack=-CurrentChangeZtrackerBack; 
        end
        changeZtrackerBack(currSample-1)= CurrentChangeZtrackerBack;
        
    end
    
    previousPointTrackerOnly = dataTrackerOnly(currSample,1:3);
    previousPointTrackerTop = dataTrackerTop(currSample,1:3);
    previousPointTrackerBack = dataTrackerBack(currSample,1:3);
    
end

meanTop = mean(samplesTop);
meanTrackerOnly = mean(samplesTrackerOnly);
meanBack = mean(samplesBack);

dfcTop = 0;
dfcBack = 0;
dfcTrackerOnly = 0;

for currSample = 1:numSamples
    dfcTop = dfcTop + norm(meanTop - samplesTop(currSample,1:3));
    dfcBack = dfcBack + norm(meanBack - samplesBack(currSample,1:3));
    dfcTrackerOnly = dfcTrackerOnly + norm(meanTrackerOnly - samplesTrackerOnly(currSample,1:3));
end

dfcTop = (dfcTop / numSamples)*1000; %mm
dfcBack = (dfcBack / numSamples)*1000; %mm
dfcTrackerOnly = (dfcTrackerOnly / numSamples)*1000; %mm

figure
hold on
grid on;
title('Drift path along X and Y axis');
plot(changeXtrackerBack, changeYtrackerBack);
plot(changeXtrackerOnly, changeYtrackerOnly);
plot(changeXtrackerTop, changeYtrackerTop);
axis([-1.1 1.1 -1.1 1.1])
xlabel('X-axis (mm)');
ylabel('Y-axis (mm)');
legend('Tracker Back', 'Tracker Only', 'Tracker Top');
hold off
print([resultDir '\drift_xy.png'],'-dpng','-r300')

figure
hold on
grid on;
title('Drift path along X and Z axis');
plot(changeXtrackerBack, changeZtrackerBack);
plot(changeXtrackerOnly, changeZtrackerOnly);
plot(changeXtrackerTop, changeZtrackerTop);
axis([-1.1 1.1 -1.1 1.1])
xlabel('X-axis (mm)');
ylabel('Z-axis (mm)');
legend('Tracker Back', 'Tracker Only', 'Tracker Top');
hold off
print([resultDir '\drift_xz.png'],'-dpng','-r300')

figure
hold on
grid on;
title('Drift path along Y and Z axis');
plot(changeYtrackerBack, changeZtrackerBack);
plot(changeYtrackerOnly, changeZtrackerOnly);
plot(changeYtrackerTop, changeZtrackerTop);
axis([-1.1 1.1 -1.1 1.1])
xlabel('Y-axis (mm)');
ylabel('Z-axis (mm)');
legend('Tracker Back', 'Tracker Only', 'Tracker Top');
hold off
print([resultDir '\drift_yz.png'],'-dpng','-r300')


fprintf('total drift tracker only = %f m\n', totalDriftTrackerOnly);
fprintf('total drift tracker top = %f m\n', totalDriftTrackerTop);
fprintf('total drift tracker back = %f m\n', totalDriftTrackerBack);
fprintf('DFC tracker top = %f mm\n', dfcTop);
fprintf('DFC tracker back = %f mm\n', dfcBack);
fprintf('DFC tracker tracker-only = %f mm\n', dfcTrackerOnly);


