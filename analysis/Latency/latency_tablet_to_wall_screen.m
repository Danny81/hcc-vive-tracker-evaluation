close all
clc

load(strcat('../../data/latency/data_tablet_to_wall_screen.mat'));
resultDir    = strcat('../../results/latency/tablet_to_wall_screen/');


meanAllSamples = mean(data(:, 2));
medianAllSamples = median(data(:, 2));

%1 ==> sankey, 2 ==> graph
sankeySamples = data(:,1)==1;
sankeySamples = data(sankeySamples,:);
meanSankey = mean(sankeySamples(:, 2));
medianSankey = median(sankeySamples(:, 2));
graphSamples = data(:,1)==2;
graphSamples = data(graphSamples,:);
meanGraph = mean(graphSamples(:, 2));
medianGraph = median(graphSamples(:, 2));

allSamplesnormalDistributed = kstest(data(:,2));
sankeyNormalDistributed =  kstest(sankeySamples(:,2));
graphNormalDistributed =  kstest(graphSamples(:,2));

quantilesSankey = quantile(sankeySamples(:,2), [0.25 0.75]);
quantilesGraph = quantile(graphSamples(:,2), [0.25 0.75]);

[pgraphVSsankey, regraphVSsankey] = ranksum(graphSamples(:,2), sankeySamples(:,2));

g1 = ones(size(sankeySamples(:,2))) * 1;
g2 = ones(size(graphSamples(:,2))) * 2;
g3 = ones(size(data(:,2))) * 3;
figure();
boxplot([sankeySamples(:,2); graphSamples(:,2); data(:,2)], [g1; g2; g3], 'Labels', {'Sankey','Graph', 'All'});
xlabel('Visualization Type')
ylabel('Latency (ms)')
title('Latency Tablet-to-Wall-Screen per Tested Visualization');
print([resultDir '\tablet_to_wall_screen_latency_per_visualization_type.png'],'-dpng','-r300');

I = find(sankeySamples(:,2) > 1130);
sankeyOutlierGridPoints = sankeySamples(I,:);

I = find(graphSamples(:,2) > 1044);
graphOutlierGridPoints = graphSamples(I,:);

I = find(data(:,3) == 0 & data(:,4) == 0);
X0Z0 = data(I,:);
medianX0Z0 = median(X0Z0(:, 2));
I = find(data(:,3) == 1 & data(:,4) == 0);
X1Z0 = data(I,:);
medianX1Z0 = median(X1Z0(:, 2));
I = find(data(:,3) == 2 & data(:,4) == 0);
X2Z0 = data(I,:);
medianX2Z0 = median(X2Z0(:, 2));
I = find(data(:,3) == -1 & data(:,4) == 0);
X_1Z0 = data(I,:);
medianX_1Z0 = median(X_1Z0(:, 2));
I = find(data(:,3) == -2 & data(:,4) == 0);
X_2Z0= data(I,:);
medianX_2Z0 = median(X_2Z0(:, 2));

I = find(data(:,3) == 0 & data(:,4) == 1);
X0Z1 = data(I,:);
medianX0Z1 = median(X0Z1(:, 2));
I = find(data(:,3) == 1 & data(:,4) == 1);
X1Z1 = data(I,:);
medianX1Z1 = median(X1Z1(:, 2));
I = find(data(:,3) == 2 & data(:,4) == 1);
X2Z1 = data(I,:);
medianX2Z1 = median(X2Z1(:, 2));
I = find(data(:,3) == -1 & data(:,4) == 1);
X_1Z1 = data(I,:);
medianX_1Z1 = median(X_1Z1(:, 2));
I = find(data(:,3) == -2 & data(:,4) == 1);
X_2Z1= data(I,:);
medianX_2Z1 = median(X_2Z1(:, 2));

I = find(data(:,3) == 0 & data(:,4) == 1.5);
X0Z15 = data(I,:);
medianX0Z15 = median(X0Z15(:, 2));
I = find(data(:,3) == 1 & data(:,4) == 1.5);
X1Z15 = data(I,:);
medianX1Z15 = median(X1Z15(:, 2));
I = find(data(:,3) == -1 & data(:,4) == 1.5);
X_1Z15 = data(I,:);
medianX_1Z15 = median(X_1Z15(:, 2));

medianPerCoordinate = [NaN medianX_1Z15 medianX0Z15 medianX1Z15 NaN; medianX_2Z1 medianX_1Z1 medianX0Z1 medianX1Z1 medianX2Z1; medianX_2Z0 medianX_1Z0 medianX0Z0 medianX1Z0 medianX2Z0];

xlabels = {-2, -1, 0, 1, 2};
ylabelsTabletToWallDispay = {1.5, 1, 0};

figure
h = heatmap(xlabels, ylabelsTabletToWallDispay, medianPerCoordinate);
caxis([860 940]);
h.Title = 'Median Latency Tablet-to-Wall-Screen per Grid Point';
h.XLabel = 'X-Coordinate';
h.YLabel = 'Z-Coordinate';
print([resultDir '\latency_tablet_to_wall_screen_PerCoord.png'],'-dpng','-r300')

I = find(sankeyOutlierGridPoints(:,3) == 0 & sankeyOutlierGridPoints(:,4) == 0);
X0Z0 = size(I,1);
I = find(sankeyOutlierGridPoints(:,3) == 1 & sankeyOutlierGridPoints(:,4) == 0);
X1Z0 = size(I,1);
I = find(sankeyOutlierGridPoints(:,3) == 2 & sankeyOutlierGridPoints(:,4) == 0);
X2Z0 = size(I,1);
I = find(sankeyOutlierGridPoints(:,3) == -1 & sankeyOutlierGridPoints(:,4) == 0);
X_1Z0 = size(I,1);
I = find(sankeyOutlierGridPoints(:,3) == -2 & sankeyOutlierGridPoints(:,4) == 0);
X_2Z0= size(I,1);

I = find(sankeyOutlierGridPoints(:,3) == 0 & sankeyOutlierGridPoints(:,4) == 1);
X0Z1 = size(I,1);
I = find(sankeyOutlierGridPoints(:,3) == 1 & sankeyOutlierGridPoints(:,4) == 1);
X1Z1 = size(I,1);
I = find(sankeyOutlierGridPoints(:,3) == 2 & sankeyOutlierGridPoints(:,4) == 1);
X2Z1 = size(I,1);
I = find(sankeyOutlierGridPoints(:,3) == -1 & sankeyOutlierGridPoints(:,4) == 1);
X_1Z1 = size(I,1);
I = find(sankeyOutlierGridPoints(:,3) == -2 & sankeyOutlierGridPoints(:,4) == 1);
X_2Z1= size(I,1);

I = find(sankeyOutlierGridPoints(:,3) == 0 & sankeyOutlierGridPoints(:,4) == 1.5);
X0Z15 = size(I,1);
I = find(sankeyOutlierGridPoints(:,3) == 1 & sankeyOutlierGridPoints(:,4) == 1.5);
X1Z15 = size(I,1);
I = find(sankeyOutlierGridPoints(:,3) == -1 & sankeyOutlierGridPoints(:,4) == 1.5);
X_1Z15 = size(I,1);

outliersPerCoordinateSankey = [NaN X_1Z15 X0Z15 X1Z15 NaN; X_2Z1 X_1Z1 X0Z1 X1Z1 X2Z1; X_2Z0 X_1Z0 X0Z0 X1Z0 X2Z0];

figure
h = heatmap(xlabels, ylabelsTabletToWallDispay, outliersPerCoordinateSankey);
caxis([0 3]);
h.Title = 'Number Latency Outliers Tablet-to-Wall-Screen Sankey Diagram per Grid Point';
h.XLabel = 'X-Coordinate';
h.YLabel = 'Z-Coordinate';
print([resultDir '\latency_outliers_per_coordinate_sankey.png'],'-dpng','-r300')


I = find(graphOutlierGridPoints(:,3) == 0 & graphOutlierGridPoints(:,4) == 0);
X0Z0 = size(I,1);
I = find(graphOutlierGridPoints(:,3) == 1 & graphOutlierGridPoints(:,4) == 0);
X1Z0 = size(I,1);
I = find(graphOutlierGridPoints(:,3) == 2 & graphOutlierGridPoints(:,4) == 0);
X2Z0 = size(I,1);
I = find(graphOutlierGridPoints(:,3) == -1 & graphOutlierGridPoints(:,4) == 0);
X_1Z0 = size(I,1);
I = find(graphOutlierGridPoints(:,3) == -2 & graphOutlierGridPoints(:,4) == 0);
X_2Z0= size(I,1);

I = find(graphOutlierGridPoints(:,3) == 0 & graphOutlierGridPoints(:,4) == 1);
X0Z1 = size(I,1);
I = find(graphOutlierGridPoints(:,3) == 1 & graphOutlierGridPoints(:,4) == 1);
X1Z1 = size(I,1);
I = find(graphOutlierGridPoints(:,3) == 2 & graphOutlierGridPoints(:,4) == 1);
X2Z1 = size(I,1);
I = find(graphOutlierGridPoints(:,3) == -1 & graphOutlierGridPoints(:,4) == 1);
X_1Z1 = size(I,1);
I = find(graphOutlierGridPoints(:,3) == -2 & graphOutlierGridPoints(:,4) == 1);
X_2Z1= size(I,1);

I = find(graphOutlierGridPoints(:,3) == 0 & graphOutlierGridPoints(:,4) == 1.5);
X0Z15 = size(I,1);
I = find(graphOutlierGridPoints(:,3) == 1 & graphOutlierGridPoints(:,4) == 1.5);
X1Z15 = size(I,1);
I = find(graphOutlierGridPoints(:,3) == -1 & graphOutlierGridPoints(:,4) == 1.5);
X_1Z15 = size(I,1);

outliersPerCoordinateGraph = [NaN X_1Z15 X0Z15 X1Z15 NaN; X_2Z1 X_1Z1 X0Z1 X1Z1 X2Z1; X_2Z0 X_1Z0 X0Z0 X1Z0 X2Z0];

figure
h = heatmap(xlabels, ylabelsTabletToWallDispay, outliersPerCoordinateGraph);
caxis([0 6]);
h.Title = 'Number Latency Outliers Tablet-to-Wall-Screen Graph Visualization per Grid Point';
h.XLabel = 'X-Coordinate';
h.YLabel = 'Z-Coordinate';
print([resultDir '\latency_outliers_per_coordinate_graph.png'],'-dpng','-r300')

fprintf('Mean all samples = %f \n', meanAllSamples);
fprintf('Median all samples = %f ms\n', medianAllSamples);
fprintf('Mean Sankey = %f \n', meanSankey);
fprintf('Median Sankey = %f ms\n', medianSankey);
fprintf('25-percent quantile Sankey = %f ms\n', quantilesSankey(1));
fprintf('75-percent quantile Sankey = %f ms\n', quantilesSankey(2));
fprintf('Mean Graph = %f \n', meanGraph);
fprintf('Median Graph = %f ms\n', medianGraph);
fprintf('25-percent quantile Graph = %f ms\n', quantilesGraph(1));
fprintf('75-percent quantile Graph = %f ms\n', quantilesGraph(2));
fprintf('Normal distribution all samples = %f \n', allSamplesnormalDistributed);
fprintf('Normal distribution Sankey = %f \n', sankeyNormalDistributed);
fprintf('Normal distribution Graph= %f \n', graphNormalDistributed);

fprintf('Difference between sankey and graph samples statistically significant = %f with p-value %f \n', regraphVSsankey, pgraphVSsankey);



