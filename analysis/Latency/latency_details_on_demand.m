close all
clc;

load(strcat('../../data/latency/data_details_on_demand.mat'));
resultDir    = strcat('../../results/latency/details_on_demand/');

x0 = data(:,3)==0;
x0 = data(x0, :);

x1 = data(:,3)==1;
x1 = data(x1, :);

x_1 = data(:,3)==-1;
x_1 = data(x_1, :);

[px0x1, relx0x1] = ranksum(x0(:,1), x1(:,1));
[px0x_1, relx0x_1] = ranksum(x0(:,1), x_1(:,1));
[px1x_1, relx1x_1] = ranksum(x1(:,1),x_1(:,1));

meanAllSamples = mean(data(:, 1));
medianAllSamples = median(data(:, 1));

meanX0 = mean(x0(:, 1));
medianX0 = median(x0(:, 1));

meanX1 = mean(x1(:, 1));
medianX1 = median(x1(:, 1));

meanX_1 = mean(x_1(:, 1));
medianX_1 = median(x_1(:, 1));

boxplot(data(:, 1));
xlabel('X-Coordinate')
ylabel('latency time (ms)')
title('Latency time Details-on-Demand in total');
print([resultDir '\details_on_demand_latency_total.png'],'-dpng','-r300');

quantilesTotal = quantile(data(:,1), [0.25 0.75]);
allSamplesnormalDistributed = kstest(data(:,1));
quantilesX_1 = quantile(x_1(:,1), [0.25 0.75]);
x_1normalDistributed = kstest(x_1(:,1))
quantilesX0 = quantile(x0(:,1), [0.25 0.75]);
X0normalDistributed = kstest(x0(:,1))
quantilesX1 = quantile(x1(:,1), [0.25 0.75]);
X1normalDistributed = kstest(x1(:,1))

boxplot(data(:, 1), data(:,3));
xlabel('X-Coordinate')
ylabel('latency time (ms)')
title('Latency time Details-on-Demand per grid location');
print([resultDir '\details_on_demand_latency_per_coordinate.png'],'-dpng','-r300');

scatter(dataRandomlySelectedNodes(:, 2), dataRandomlySelectedNodes(:, 1));
xlabel('Number of nieghbour nodes')
ylabel('latency time (ms)')
title('Correlation between Latency and number of neigbour nodes');
print([resultDir '\correlation_latency_neighbur_nodes_random.png'],'-dpng','-r300');

R = corrcoef(dataRandomlySelectedNodes);

all = [data(:, 1:2); dataRandomlySelectedNodes];
scatter(all(:, 2), all(:, 1));
xlabel('Number of nieghbour nodes')
ylabel('latency time (ms)')
title('Correlation between Latency and number of neigbour nodes');
print([resultDir '\correlation_latency_neighbur_nodes_all.png'],'-dpng','-r300');

R2 = corrcoef(all);

fprintf('normal distribution over all samples = %f ms\n', allSamplesnormalDistributed);

fprintf('Mean all samples = %f \n', meanAllSamples);
fprintf('Median all samples = %f ms\n', medianAllSamples);
fprintf('25-percent quantile all samples = %f ms\n', quantilesTotal(1));
fprintf('75-percent quantile all samples = %f ms\n', quantilesTotal(2));

fprintf('Mean X -1 = %f ms\n', meanX_1);
fprintf('Median X -1 = %f ms\n', medianX_1);
fprintf('25-percent quantile x-1 = %f ms\n', quantilesX_1(1));
fprintf('75-percent quantile x-1 = %f ms\n', quantilesX_1(2));
fprintf('Mean X0 = %f ms\n', meanX0);
fprintf('Median X0 samples = %f ms\n', medianX0);
fprintf('25-percent quantile x0 = %f ms\n', quantilesX0(1));
fprintf('75-percent quantile x0 = %f ms\n', quantilesX0(2));
fprintf('Mean X1 samples = %f ms\n', meanX1);
fprintf('Median X1 samples = %f ms\n', medianX1);
fprintf('25-percent quantile x1 = %f ms\n', quantilesX1(1));
fprintf('75-percent quantile x1 = %f ms\n', quantilesX1(2));

