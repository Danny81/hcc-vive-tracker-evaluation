close all
clc

load(strcat('../../data/stability/dynamic_stability.mat'));
resultDir    = strcat('../../results/stability/dynamic_stability/');

trackerTopnormalDistributed = kstest(data_tracker_top(:,:));
trackerBacknormalDistributed = kstest(data_tracker_back(:,:));

trackerTopFramesTrackingLost= [];
trackerBackFramesTrackingLost= [];

checkCalcTop = 0;
checkCalcBack = 0;

for j=1:size(data_tracker_top, 1)
    if j > 1
       lastSample =  data_tracker_top(j-1, 1:6);
       
       if lastSample == data_tracker_top(j,1:6)        
            trackerTopFramesTrackingLost=[trackerTopFramesTrackingLost; lastSample];
       end
       if data_tracker_top(j,7) == 0 
           checkCalcTop = checkCalcTop+1;
       end
    end
end


for k=1:size(data_tracker_back, 1)
    if k > 1
       lastSample =  data_tracker_back(k-1, 1:6);
       
       if lastSample == data_tracker_back(k,1:6)        
            trackerBackFramesTrackingLost=[trackerBackFramesTrackingLost; lastSample];
       end
       if data_tracker_back(k,7) == 0 
           checkCalcBack = checkCalcBack+1;
       end
    end
end

temp = size(trackerTopFramesTrackingLost());
trackerTopTotalFramesTrackingLost = temp(1);
checkCalcTop = 100 - ((100 / j)*checkCalcTop); 

temp = size(trackerBackFramesTrackingLost());
trackerBackTotalFramesTrackingLost = temp(1);
checkCalcBack = 100 - ((100 / k)*checkCalcBack); 

stabilityTrackerTop = 100 - ((100 / j)*trackerTopTotalFramesTrackingLost); 
stabilityTrackerBack = 100 - ((100 / k)*trackerBackTotalFramesTrackingLost); 

fprintf('Tracker Top normal distributed = %f\n', trackerTopnormalDistributed);
fprintf('Tracker Back normal distributed = %f\n', trackerBacknormalDistributed);

fprintf('Stability tracker Top = %f%%\n', stabilityTrackerTop);
fprintf('Stability tracker Back = %f%%\n', stabilityTrackerBack);

[p,h] = ranksum(data_tracker_top(:),data_tracker_back(:));

trackingLostTop = data_tracker_top(:,7) == 0;
trackingLostTop = data_tracker_top(trackingLostTop,:);

trackingLostTopUnique = unique(trackingLostTop, 'rows');

[gridx,gridz] = meshgrid(-2:2,1.5:-0.5:-1.5);
clf, hold on
plot(trackingLostTopUnique(:,1), trackingLostTopUnique(:,3), 'bx');
plot([-1 1], [1.9 1.9], 'r')
xlabel('X (m)');
ylabel('Z (m)');
title('Last Reported Position Before Loss of Tracking Tracker Top');
axis([-2 2 -2 2])
axis equal
grid on

print([resultDir '\dynamic_reliability_trackerTop_xz.png'],'-dpng','-r300');
trackingLostBack = data_tracker_back(:,7) == 0;
trackingLostBack = data_tracker_back(trackingLostBack,:);

trackingLostBackUnique = unique(trackingLostBack, 'rows');

[gridx,gridz] = meshgrid(-2:2,1.5:-0.5:-1.5);
clf, hold on
plot(trackingLostBackUnique(:,1), trackingLostBackUnique(:,3), 'bx');
plot([-1 1], [1.9 1.9], 'r')
xlabel('X (m)');
ylabel('Z (m)');
title('Last Reported Position Before Loss of Tracking Tracker Backside');
axis([-2.5 2.5 -2 2])
grid on

print([resultDir '\dynamic_reliability_trackerBack_xz.png'],'-dpng','-r300');

