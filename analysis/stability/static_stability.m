close all
clc

load(strcat('../../data/stability/static_stability.mat'));
resultDir    = strcat('../../results/stability/static_stability/');

trackerOnlynormalDistributed = kstest(data_tracker_only(:,:));
trackerTopnormalDistributed = kstest(data_tracker_top(:,:));
trackerBacknormalDistributed = kstest(data_tracker_back(:,:));

trackerOnlyFramesTrackingLost= [];
trackerTopFramesTrackingLost= [];
trackerBackFramesTrackingLost= [];

for i=1:size(data_tracker_only, 1)
    if i > 1
       lastSample =  data_tracker_only(i-1, :);
       
       if lastSample == data_tracker_only(i,:)        
            trackerOnlyFramesTrackingLost=[trackerOnlyFramesTrackingLost; lastSample];
       end
    end
end

for j=1:size(data_tracker_top, 1)
    if j > 1
       lastSample =  data_tracker_top(j-1, :);
       
       if lastSample == data_tracker_top(j,:)        
            trackerTopFramesTrackingLost=[trackerTopFramesTrackingLost; lastSample];
       end
    end
end


for k=1:size(data_tracker_back, 1)
    if k > 1
       lastSample =  data_tracker_back(k-1, :);
       
       if lastSample == data_tracker_back(k,:)        
            trackerBackFramesTrackingLost=[trackerBackFramesTrackingLost; lastSample];
       end
    end
end


temp = size(trackerOnlyFramesTrackingLost());
trackerOnlyTotalFramesTrackingLost = temp(1);

temp = size(trackerTopFramesTrackingLost());
trackerTopTotalFramesTrackingLost = temp(1);

temp = size(trackerBackFramesTrackingLost());
trackerBackTotalFramesTrackingLost = temp(1);

stabilityTrackerOnly = 100 - ((100 / i)*trackerOnlyTotalFramesTrackingLost); 
stabilityTrackerTop = 100 - ((100 / j)*trackerTopTotalFramesTrackingLost); 
stabilityTrackerBack = 100 - ((100 / k)*trackerBackTotalFramesTrackingLost); 

fprintf('Tracker only normal distributed = %f\n', trackerOnlynormalDistributed);
fprintf('Tracker Top normal distributed = %f\n', trackerTopnormalDistributed);
fprintf('Tracker Back normal distributed = %f\n', trackerBacknormalDistributed);

fprintf('Stability tracker only = %f%%\n', stabilityTrackerOnly);
fprintf('Stability tracker Top = %f%%\n', stabilityTrackerTop);
fprintf('Stability tracker Back = %f%%\n', stabilityTrackerBack);


